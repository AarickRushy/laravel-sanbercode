@extends('layout.master')
@section('title')
    Halaman Register Biodata
@endsection

@section('subtitle')
    Buat Account Baru!<br>
    Sign Up Form
@endsection

@section('content')
    <form action="/submit" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="firstName"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="lastName"><br><br>
        <label>Password :</label><br><br>
        <input type="password" name="pass"><br><br>
        <label>Gender :</label><br><br>
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br>
        <input type="radio" name="gender" value="other">Other <br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality">
            <option value="1">Indonesia</option>
            <option value="2">English</option>
            <option value="3">Jawa</option>
        </select> <br><br>        
        <label>Languange Spoken :</label><br><br>
        <input type="checkbox" name="language_spoken" value="1">Indonesia <br>
        <input type="checkbox" name="language_spoken" value="2">English <br>
        <input type="checkbox" name="language_spoken" value="3">France <br><br>
        <label>Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        
        <input type="submit" value="Sign Up"><br><br>
    </form>

    <a href="{{url('/')}}">Kembali</a>
@endsection
   